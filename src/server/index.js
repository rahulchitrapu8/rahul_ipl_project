const csv = require('csvtojson')
const fs = require ('fs');

const csvFilePath1='../data/deliveries.csv';
const csvFilePath2='../data/matches.csv'
const {getMatchesPlayedPerSeason,matchesWonPerTeamPerYear,extraRunsPerTeam,topEconomicBowlersInYear}=require("./ipl2")


csv()
.fromFile(csvFilePath1)
.then((jsonObj)=>{
    // console.log(jsonObj);
    fs.writeFile('../public/output/deliveries.json', JSON.stringify(jsonObj, null, 4), (err)=>{
    if(err){
        throw err;
    }
})
});
csv()
.fromFile(csvFilePath2)
.then((jsonObj)=>{
    // console.log(jsonObj);
    fs.writeFile('../public/output/matches.json', JSON.stringify(jsonObj, null, 4), (err)=>{
        if(err){
            throw err;
        }
    })
})


//calling function
const matches=require("../public/output/matches.json");
const deliveries=require("../public/output/deliveries.json")
const { match } = require('assert');

const macthes_per_season=getMatchesPlayedPerSeason(matches)
fs.writeFile('../public/output/matches_per_season_des.json', JSON.stringify(macthes_per_season, null, 4), (err)=>{
    if(err){
        throw err;
    }
    else{
        console.log('matches_per_season_des.json file added succesfully')
    }
})
const matchesWonPerTeamPerYearOP=matchesWonPerTeamPerYear(matches)
fs.writeFile('../public/output/matches_won_per_season_per_team_des.json', JSON.stringify(matchesWonPerTeamPerYearOP, null, 4), (err)=>{
    if(err){
        throw err;
    }
    else{
        console.log('matches_won_per_season_per_team_des.json file added succesfully')
    }
})

fs.writeFile('../public/output/extra_runs_per_team_2016.json', JSON.stringify(extraRunsPerTeam(matches,deliveries,2016), null, 4), (err)=>{
    if(err){
        throw err;
    }
    else{
        console.log('extra_runs_per_team_2016.json file added succesfully')
    }
})

fs.writeFile('../public/output/top_economical_bowler_2015.json', JSON.stringify(topEconomicBowlersInYear(matches,deliveries,2015), null, 4), (err)=>{
    if(err){
        throw err;
    }
    else{
        console.log('top_economical_bowler_2015.json file added succesfully')
    }
})
