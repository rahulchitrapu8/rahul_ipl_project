const matchObj=require(`../public/output/matches.json`)
const fs=require("fs")
const deliveriesObj=require(`../public/output/deliveries.json`)
const { match } = require("assert")
// number of matches played in an year
let matchesPLayed={}
for(let i=0;i<matchObj.length;i++){
    let key=matchObj[i]['season']
   
    matchesPLayed[key]=matchesPLayed[key]+1||1
    
}
fs.writeFile('../public/output/matchesPlayed.json',JSON.stringify(matchesPLayed,null,4),(err)=>{
    if(err){
        throw err
    }
})



// number of matches won by a team in an year
let wonTeams={}
for(let i in matchObj){
    let year=matchObj[i]['season']
    if(!wonTeams[year]){
        wonTeams[year]={}
    }
    // let year=matchObj[i]['season']
    let winner=matchObj[i]['winner']
    wonTeams[year][winner]=wonTeams[year][winner]+1||1

}

fs.writeFile(`../public/output/matchesWonInanYear.json`,JSON.stringify(wonTeams,null,4),(err)=>{
    if(err){
        throw err
    }
})


// in 2016 extra runs conceeded by each team

let extraRuns={}

let matchIDs=[]

for(let i of matchObj){
    if(i.season==='2016'){
        matchIDs.push(i.id)
    }
}



for(let i of deliveriesObj){
    if(matchIDs.includes(i.match_id)){
       extraRuns[i.bowling_team]=extraRuns[i.bowling_team]+parseInt(i.extra_runs)||parseInt(i.extra_runs)
    }
}

fs.writeFile(`../public/output/extra_runs.json`,JSON.stringify(extraRuns,null,4),(err)=>{
    if(err){
        throw err
    }
})




// top economic bowlers 2015

let bowlerObj={}

let matchID=[]

for(let i of matchObj){
    if(i.season==='2015'){
        matchID.push(i.id)
    }
}

for(let i of deliveriesObj){
    if(matchID.includes(i.match_id)){

    
        let bowler=i.bowler
        if(!bowlerObj[bowler]){

            bowlerObj[bowler]={}
        }
        bowlerObj[bowler]["runs"]=bowlerObj[bowler]["runs"]+parseInt(i.total_runs)||parseInt(i.total_runs)
        bowlerObj[bowler]["balls"]=bowlerObj[bowler]["balls"]+1||1
        bowlerObj[bowler]["economy"]=bowlerObj[bowler]["runs"]/((bowlerObj[bowler]["balls"])/6)
    }
}

let out = []
for(let key in bowlerObj){
    out.push([bowlerObj[key]["economy"],key])
}


out.sort((a,b) => a[0] - b[0])


let topEconomicBowlers=out.slice(0,10)
let obj=[]
topEconomicBowlers.forEach((e)=>{
    obj.push({player_name:e[1],economy:Math.round(e[0])})

})


fs.writeFile(`../public/output/topEconomicBowlers.json`,JSON.stringify(obj,null,4),(err)=>{
    if(err){
        throw err
    }
})




