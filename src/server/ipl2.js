 function hello(){
    return ("hello")
}

const matches=require("../public/output/matches.json")
const deliveries=require("../public/output/deliveries.json")

function getMatchesPlayedPerSeason(matches){
    let arr=matches.map(match=>match.season)
    return (arr.reduce((result,season)=>{
        if(result[season]){
            result[season]+=1
        }else{
            result[season]=1
        }
        return result
    },{}))

    
}

function matchesWonPerTeamPerYear(matches){
    let years=matches.map((match)=>{
        return {season:match.season,winner:match.winner}
    })
    
    let finalObj=years.reduce((res,obj)=>{
        let year=obj.season
        let winner=obj.winner
        if(!res[year]){
            res[year]={}
        }
        res[year][winner]=res[year][winner]+1||1
        
        return res
    },{})
    return (finalObj)

}
function getMatchID(matches,year){
    // let matchID=[]
   
    // matches.filter((match)=>{
    //     if(match.season==year){    
    //         matchID.push(match.id)
    //     }
    // })
   return (matches.filter(e=>e.season==year).map((e)=>{
        return e.id
    }))
    
    // return matchID
}

function extraRunsPerTeam(matches,deliveries,year){
    let matchID=getMatchID(matches,year)
    // let extraRuns={}
    // matches.filter((match)=>{
    //     if(match.season==year){    
    //         matchID.push(match.id)
    //     }
    // })
    // deliveries.filter((delivery)=>{
        
    //     if(matchID.includes(delivery["match_id"])){
    //         if(extraRuns[delivery.bowling_team]){
    //             extraRuns[delivery.bowling_team]+=parseInt(delivery.extra_runs)
    //         }
    //         else{
    //             extraRuns[delivery.bowling_team]=parseInt(delivery.extra_runs)
    //         }
            
    //     }
    // })
    let extraRuns=deliveries.reduce((obj,delivery)=>{
        if(matchID.includes(delivery.match_id)){
            if(obj[delivery.bowling_team]){
                obj[delivery.bowling_team]+=parseInt(delivery.extra_runs)
            }
            else{
                obj[delivery.bowling_team]=parseInt(delivery.extra_runs)
            }
            
        }
        return obj
    },{})
    return (extraRuns)
    
}

function topEconomicBowlersInYear(matches,deliveries,year){
    let matchID=getMatchID(matches,year)
    let bowlingObj=deliveries.reduce((obj,delivery)=>{
        if(matchID.includes(delivery.match_id)){
            if(!obj[delivery.bowler]){
                obj[delivery.bowler]={}
            }
            obj[delivery.bowler]["runs"]=obj[delivery.bowler]["runs"]+parseInt(delivery.total_runs)||parseInt(delivery.total_runs)
            obj[delivery.bowler]["balls"]=obj[delivery.bowler]["balls"]+1||1
            obj[delivery.bowler]["economy"]=obj[delivery.bowler]["runs"]/((obj[delivery.bowler]["balls"])/6)
        }
        return obj
    },{})
    
    let top_10=Object.entries(bowlingObj).sort((a,b)=> a[1].economy - b[1].economy ).splice(0, 10)
    let top_10_players=top_10.reduce((obj,player)=>{
        let player_name=player[0]
        let playerObj={playerName:player_name,economy:Math.round(player[1].economy)}
        obj[player_name]=playerObj
    
        return obj
       
    },{})
    
    return (Object.values(top_10_players))
}


// topEconomicBowlersInYear(matches,deliveries,2015)
// console.log(extraRunsPerTeam(matches,deliveries,2016))
// console.log(getMatchID(matches,2015))


module.exports={
    getMatchesPlayedPerSeason,
    matchesWonPerTeamPerYear,
    extraRunsPerTeam,
    topEconomicBowlersInYear
}